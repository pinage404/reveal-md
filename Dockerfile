FROM node:20-alpine
ENTRYPOINT []
CMD ["/bin/sh"]

ENV CI=true

WORKDIR /reveal-md-with-plugins
RUN chown node /reveal-md-with-plugins
USER node

COPY package.json .
COPY package-lock.json .
RUN npm ci --omit=dev

COPY bin/ bin/
USER root
RUN sed -i "s@\\./@$(realpath .)/@g" bin/reveal-md-with-plugins
RUN ln -s /reveal-md-with-plugins/bin/reveal-md-with-plugins /usr/local/bin/
USER node

COPY reveal_plugins/ reveal_plugins/
