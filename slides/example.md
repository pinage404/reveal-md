---
title: Example
verticalSeparator: ^----$
---

# Example

---

## Plugins

----

### Mermaid

```mermaid
%%{init: {'theme':'dark'}}%%
flowchart LR
    Start --> Stop
```

----

#### Verticator

The buttons in the right side to change between vertical slides

➡️

----

#### Chalkboard

The buttons in the bottom left corner to draw stuff

↙️

---

<!-- markdownlint-disable -->
<p class="legal">
    <a rel="license" href="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.en">
        <img alt="Licence Creative Commons BY-NC-SA" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" />
    </a>
    by
    <a rel="author external" href="https://wheretofind.me/@pinage404">
        <img alt="pinage404" src="https://s.gravatar.com/avatar/0a7d96df27d5d020cb0d03340e734180?s=40" />
    </a>
    on
    <a rel="alternate" href="https://gitlab.com/pinage404/initiation_git">
        <img alt="GitLab" src="https://img.shields.io/gitlab/stars/pinage404/initiation_git?style=social" />
    </a>
</p>
<style>
.legal img {
    margin: 0;
    vertical-align: middle;
}
</style>
