# Reveal-md with plugins

---

[VSCode Reveal's plugin](https://marketplace.visualstudio.com/items?itemName=evilz.vscode-reveal)
and
[reveal-md](https://github.com/webpro/reveal-md)
are almost the same ;
[unfortunately, the tools are different](https://github.com/hakimel/reveal.js/issues/1934)


This Reveal-md with plugins is of poorer quality compared to VScode Reveal's plugin

So, [i switched](https://pinage404.gitlab.io/slides/) to [slidev](https://sli.dev)

---

## Usage

In `.gitlab-ci.yml` add:

```yaml
include:
  - remote: "https://gitlab.com/pinage404/reveal-md/raw/main/.gitlab/ci/pages.gitlab-ci.yml"
```
